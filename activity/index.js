const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://x3rubie:IzzyBooBoo0214!@cluster0.z8dag7j.mongodb.net/MONGOOSE?retryWrites=true&w=majority", 
	{	
		
		useNewUrlParser : true,
		useUnifiedTopology : true
	}

);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.on('open', () => console.log("Connected to MongoDB!"));

const userSchema = new mongoose.Schema({ 

	username : String,
	password : String,
});

const User = mongoose.model("User", userSchema);

app.post('/signup', (req, res) => {

	User.findOne({username : req.body.username}, (err, result) => {

		if(result != null && result.username == req.body.username){

			return res.send("Duplicate user found");

		} else {

			let newUser = new User({
				username : req.body.username,
				password : req.body.password
			});

			newUser.save((saveErr, savedUser) => {

				if(saveErr){

					return console.error(saveErr);

				} else {

					return res.status(201).send("New user registered!");

				}
			})
		}

	})
});

app.get("/users", (req, res) => {

	User.find({}, (err, result) => {

		if (err) {

			return console.log(err);

		} else {

			return res.status(200).json({
				data : result			
			})

		}

	})
});





app.listen(port, () => console.log(`Server running at port ${port}!`))